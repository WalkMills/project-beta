import TechnicianList from './Components/Technicians/TechnicianList';
import TechForm from './Components/Technicians/TechnicianForm';
import AppointmentForm from './Components/Appointments/AppointmenFormt';
import AppointmentList from './Components/Appointments/AppointmentList';
import ServiceHistory from './Components/Appointments/ServiceHistory';
import ManufacturerForm from './Components/Manufacturers/ManufacturerForm';
import ManufacturerList from './Components/Manufacturers/ManufacturerList';
import VehicleModelForm from './Components/Vehicle Models/VehicleModelForm';
import VehicleModelList from './Components/Vehicle Models/VehicleModelList';
import AutomobileForm from './Components/Automobiles/AutomobileForm';
import AutomobileList from './Components/Automobiles/AutomobileList';
import CustomerForm from './SalesComponents/Customer/CustomerForm'
import CustomerList from './SalesComponents/Customer/CustomerList'
import SalesForm from './SalesComponents/Sales/SalesForm'
import SalesHistory from './SalesComponents/Sales/SalesHistory'
import SalesList from './SalesComponents/Sales/SalesList'
import SalespeopleList from './SalesComponents/Salesperson/SalespeopleList'
import SalespersonForm from './SalesComponents/Salesperson/SalespersonForm'

export {
    TechForm,
    TechnicianList,
    AppointmentForm,
    AppointmentList,
    ServiceHistory,
    ManufacturerForm,
    ManufacturerList,
    VehicleModelForm,
    VehicleModelList,
    AutomobileForm,
    AutomobileList,
    CustomerForm,
    CustomerList,
    SalesForm,
    SalesHistory,
    SalesList,
    SalespeopleList,
    SalespersonForm
}
