import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import {
  TechForm, TechnicianList,
  AppointmentForm, AppointmentList,
  ServiceHistory, ManufacturerForm,
  ManufacturerList, VehicleModelForm,
  VehicleModelList, AutomobileForm,
  AutomobileList, CustomerForm,
  CustomerList, SalesForm,
  SalesHistory, SalesList,
  SalespeopleList, SalespersonForm

  } from './components'



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/">
            <Route index element={<MainPage />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianList/>}/>
            <Route path="new" element={<TechForm/>}/>
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList/>}/>
            <Route path="new" element={<AppointmentForm/>}/>
            <Route path="history" element={<ServiceHistory/>} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList/>}/>
            <Route path="new" element={<ManufacturerForm/>}/>
            <Route index element={<ManufacturerList/>}/>
          </Route>
          <Route path="vehicleModels">
            <Route index element={<VehicleModelList/>}/>
            <Route path="new" element={<VehicleModelForm/>}/>
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList/>}/>
            <Route path="new" element={<AutomobileForm/>}/>
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList/>}/>
            <Route path="new" element={<CustomerForm/>}/>
          </Route>
          <Route path="sales">
            <Route index element={<SalesList/>}/>
            <Route path="new" element={<SalesForm/>}/>
            <Route path="history" element={<SalesHistory/>}/>
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList/>}/>
            <Route path="new" element={<SalespersonForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
