import {useState, useEffect } from "react"

function SalesHistory () {
    const [sales, setSales] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [salesList, setSalesList] = useState([])

    const fetchSalespeople = async () => {
        const res = await fetch("http://localhost:8090/api/salespeople/")
        if (res.ok) {
        const data = await res.json()
        setSalespeople(data.salespeople)
    } else {
        console.error(res)
        }
    }

    const fetchSales = async () => {
        const res = await fetch("http://localhost:8090/api/sales/")

        if (res.ok) {
            const data = await res.json()
            setSales(data.sales)
            setSalesList(data.sales)
        } else {
            console.error(res)
        }
    }

    useEffect(() => {
        fetchSales();
        fetchSalespeople();
    }, []);

    const handleChange = async (e) => {
        const name = e.target.value
        setSalesList(sales.filter(sale => `${sale.salesperson.first_name} ${sale.salesperson.last_name}` === name))
    }

    return (
        <div>
            <h1>Salesperson History</h1>
                <div>
                    <div className="mb-3">
                        <select required={true} name="salesperson" onChange={handleChange} className="form-select" >
                            <option value="">Salesperson</option>
                            {salespeople.map(prop => {
                                return (
                                    <option key={prop.id}>
                                        { `${prop.first_name} ${prop.last_name}` }
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesList.map((prop, i) => {
                        const evens = i % 2 === 0;
                        const backgroundColor = evens ? "#EAEAEA" : "#fff";
                        return (
                            <tr key={prop.id} style={{ backgroundColor }} >
                                <td>{ `${prop.salesperson.first_name} ${prop.salesperson.last_name}` }</td>
                                <td>{ `${prop.customer.first_name} ${prop.customer.last_name}` }</td>
                                <td>{ prop.automobile.vin }</td>
                                <td>{ prop.price }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalesHistory
// If a salesperson hasn't made any sales add a message saying no sales made (stretch)
