import { useState, useEffect } from "react"

function CustomerList () {
    const [customers, setCustomers] = useState([])

    const fetchCustomers = async () => {
        const response = await fetch("http://localhost:8090/api/customers/")

        if (response.ok) {
            const customerData = await response.json()
            setCustomers(customerData.customers)
        } else {
            console.error(response)
        }
    }

    useEffect(() => {
        fetchCustomers();
    }, []);

    return (
        <div>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {customers.map((customer, i) => {
                            const evens = i % 2 === 0;
                            const backgroundColor = evens ? "#EAEAEA" : "#fff";

                            return (
                                <tr key={ customer.first_name } style={{ backgroundColor }}>
                                    <td>{ customer.first_name }</td>
                                    <td>{ customer.last_name }</td>
                                    <td>{ customer.address }</td>
                                    <td>{ customer.phone_number }</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default CustomerList
