import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function VehicleModelForm () {

    const [manufacturers, setManus] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: ''
    })

    const loadManus = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if (response.ok){
            const data = await response.json()
            setManus(data.manufacturers)
        } else {
            console.error(response)
        }
    }

    const handleChange = async (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const response = await fetch(
            'http://localhost:8100/api/models/',
            {method: 'post', body: JSON.stringify(formData), headers: {'Content-Type': 'application/json'}}
        )
        if (response.ok){
            const newModel = response.json()
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: ''
            })
            document.getElementById("create-model-form").reset()
        }
    }

    useEffect(() => {
        loadManus();
    }, [])


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <NavLink to="/vehicleModels" end><button className="btn btn-success">Back</button></NavLink>
                    <h1>Create a Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} placeholder="Name" name="name" id="name" type="text" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange}  required={true} placeholder="Photo URL" name="picture_url" id="picture_url" type="url" className="form-control"/>
                            <label htmlFor="picture_url">Photo URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleChange}  required={true} id="manufacturer_id" name="manufacturer_id" className="form-select">
                                <option value="">Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default VehicleModelForm;
