import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function AppointmentForm () {

    const [technicians, setTechnicians] = useState([])
    const [formData, setFormData] = useState({
        customer: "",
        reason: "",
        date: "",
        time: "",
        vin: "",
        technician: "",
    })

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technician)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.customer = formData.customer
        data.reason = formData.reason
        data.vin = formData.vin
        data.date_time = formData.date + " " + formData.time
        data.technician = formData.technician

        const appointmentUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok){
            const newAppointment = await response.json()

            setFormData({
                customer: "",
                reason: "",
                date: "",
                time: "",
                vin: "",
                technician: "",
            })
            document.getElementById('schedule-appointmnet-form').reset()
        }

    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <NavLink to="/appointments" end><button className="btn btn-success">Back</button></NavLink>
                    <h1>Schedule an Appointment</h1>
                    <form onSubmit={handleSubmit} id="schedule-appointmnet-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} placeholder="Customer" type="text" name="customer" id="customer" className="form-control"/>
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} placeholder="Reason" type="text" name="reason" id="reason" className="form-control"/>
                            <label htmlFor="reason">Concise Reason For Appointment</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} placeholder="Date" type="date" name="date" id="date" className="form-control"/>
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} placeholder="Time" type="time" name="time" id="time" className="form-control"/>
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} placeholder="Vin" type="text" name="vin" id="vin" className="form-control"/>
                            <label htmlFor="vin">Vin</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleChange} required={true} placeholder="Technician" type="text" name="technician" id="technician" className="form-control">
                            <option value="">Technician</option>
                            {technicians.map(technician => {
                                return (
                                    <option key={technician.id} value={technician.id}>
                                        {technician.first_name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <button className="btn btn-success">Schedule</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;
