import { useState, useEffect } from "react";



function AppointmentList () {

    const [appointments, setAppointments] = useState([])

    const loadApps = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')

        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
        } else {
            console.error(response)
        }
    }

    const handleFinish = async (event) => {
        const AppId = event.target.value
        const url = `http://localhost:8080/api/appointments/${AppId}/finish`
        const fetchConfig = {
            method: 'put'
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setAppointments(appointments.filter(item => item.id !== Number(AppId) ))
        }
    }

    const handleCancel = async (event) => {
        const AppId = event.target.value
        const url = `http://localhost:8080/api/appointments/${AppId}/cancel`
        const fetchConfig = {
            method: 'put'
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setAppointments(appointments.filter(item => item.id !== Number(AppId) ))
        }
    }


    useEffect(() => {
        loadApps();
    }, []);

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP?</th>
                        <th>Customer</th>
                        <th>Reason</th>
                        <th>Date / Time</th>
                        <th>Technician</th>
                        <th>Update Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(item => item.status === "created").map(appointment => {
                        const date = new Date(appointment.date_time)
                        const formatDate = date.toLocaleDateString('en-US', {
                            day: '2-digit',
                            month: '2-digit',
                            year: '2-digit'
                        })
                        return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.is_vip}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.reason}</td>
                            <td>{formatDate} </td>
                            <td>{appointment.technician.first_name + " " +appointment.technician.last_name}</td>
                            <td>
                                <button style={{"marginRight": "15px"}} value={appointment.id} onClick={handleFinish} className="btn btn-outline-success">Finish</button>
                                <button value={appointment.id} onClick={handleCancel} className="btn btn-outline-danger">Cancel</button>
                            </td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentList;
