from django.urls import path

from .views import(
    api_list_customers,
    delete_customer,
    api_list_salespeople,
    delete_salesperson,
    api_list_sale,
    delete_sale
)

urlpatterns = [
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", delete_customer, name="delete_customer"),
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:pk>/", delete_salesperson, name="delete_salesperson"),
    path("sales/", api_list_sale, name="api_list_sale"),
    path("sales/<int:pk>/", delete_sale, name="delete_sale")
]
