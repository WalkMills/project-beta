from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse

from .models import Customer, Salesperson, Sale, AutomobileVO
from .encoders import(
    AutomobileVODetailEncoder,
    CustomerDetailEncoder,CustomerListEncoder,
    SalespersonDetailEncoder, SalespersonListEncoder,
    SaleDetailEncoder, SaleListEncoder
)
import json


# Customer Views
@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"},
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET"])
def delete_customer(request, pk):
    try:
        if request.method == "GET":
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
    except:
        response = JsonResponse(
            {"message": "That customer id does not exist"}
        )
        response.status_code = 400
        return response

    else:
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )


# Salesperson Views
@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalespersonDetailEncoder,
                safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "Could not create new salesperson"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET"])
def delete_salesperson(request, pk):
    try:
        if request.method == "GET":
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonDetailEncoder,
                safe=False
            )
    except:
        response = JsonResponse(
            {"message": "That salesperson does not exist"}
        )
        response.status_code = 400
        return response

    else:
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )


# Sales Views
@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:

            auto_href = content["automobile"]
            auto = AutomobileVO.objects.get(import_href=auto_href)
            content["automobile"] = auto

            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson

            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Invalid automobile id"},
            )
            response.status_code = 400
            return response
        sales = Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SaleListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def delete_sale(request, pk):
    try:
        if request.method == "GET":
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False
            )
    except:
        response = JsonResponse(
            {"message": "That sale id does not exist"}
        )
        response.status_code = 400
        return response
    else:
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
