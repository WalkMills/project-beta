from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default="")
    vin = models.CharField(max_length=50)
    sold = models.BooleanField()

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.SmallIntegerField()

class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)

class Sale(models.Model):
    price = models.CharField(max_length=50)
    automobile = models.ForeignKey(AutomobileVO, related_name="automobile", on_delete=models.CASCADE)
    salesperson = models.ForeignKey(Salesperson, related_name="salesperson", on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, related_name="customer", on_delete=models.CASCADE)
