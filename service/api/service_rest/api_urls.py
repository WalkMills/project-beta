from django.urls import path
from .views import (
    api_get_appointments,
    api_finish_appointment,
    api_cancel_appointment,
    api_get_technicians,
    api_list_appointments,
    api_list_technicians
)


urlpatterns = [
    path("technicians/", api_list_technicians, name="list technicians"),
    path("technicians/<int:id>/", api_get_technicians, name="delete technicians"),
    path("appointments/", api_list_appointments, name="list appointments"),
    path("appointments/<int:id>/", api_get_appointments, name="get appointments"),
    path("appointments/<int:id>/cancel", api_cancel_appointment, name="cancel appointment"),
    path("appointments/<int:id>/finish", api_finish_appointment, name="finish appointment")
]
