from common.json import ModelEncoder
from .models import Technician, Appointment


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "status",
        "date_time",
        "reason",
        "customer",
        "vin",
        "is_vip",
        "technician"
    ]

    encoders = {
        "technician": TechnicianListEncoder(),
    }
