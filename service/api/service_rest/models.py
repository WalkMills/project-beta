from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=150, unique=True)


class Appointment(models.Model):

    STATUS_CHOICES = (
        ("created", "created"),
        ("finished", "finished"),
        ("canceled", "canceled")
    )

    date_time = models.DateTimeField( )
    reason = models.TextField()
    status = models.CharField(choices=STATUS_CHOICES, max_length=10, default="created")
    customer = models.CharField(max_length=100)

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )
    is_vip = models.CharField(max_length=3, default="No")
    vin = models.CharField(max_length=200)

    def canceled(self):
        self.status = "canceled"
        self.save()

    def finished(self):
        self.status = "finished"
        self.save()





class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default="")
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default=False)
