from .encoders import TechnicianListEncoder, AppointmentListEncoder
from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse



@require_http_methods(["GET","POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse({"technician": technician}, encoder=TechnicianListEncoder)
    else:
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)

        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_get_technicians(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician ID"},
                status=404
            )
        return JsonResponse(
            technician,
            TechnicianListEncoder,
            safe=False
        )
    else:
        try:
            count, _ = Technician.objects.get(id=id).delete()
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician ID"},
                status=404
        )
        return JsonResponse({"deleted": count>0})


@require_http_methods(["GET","POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse({"appointments": appointment}, encoder=AppointmentListEncoder)
    else:
        content = json.loads(request.body)
        if AutomobileVO.objects.filter(vin=content["vin"], sold = True):
            content["is_vip"] = "Yes"
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=404
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_get_appointments(request,id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Appointment ID"},
                status=404
        )

        return JsonResponse(
            appointment,
            AppointmentListEncoder,
            safe=False
        )
    else:
        try:
            count, _ = Appointment.objects.get(id=id).delete()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Appointment ID"},
                status=404
        )
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Appointment ID"},
            status=404
        )

    appointment.canceled()
    return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Appointment ID"},
            status=404
        )

    appointment.finished()
    return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
